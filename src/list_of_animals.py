animals = [
    {'name': 'Burek', 'kind': 'dog', 'age': None},
    {'name': 'Kitek', 'kind': 'cat', 'age': 3},
    {'name': 'Flo', 'kind': 'fish', 'age': 1},
]
print (animals[-1]['name'])
animals[1]['age'] = 2
print (animals[1])
animals.insert(0, {'name': 'Misio', 'kind': 'hamster', 'age': 4})
print (animals)