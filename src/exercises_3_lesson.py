adresses = [
    {'city': 'Warsaw', 'street': 'Polna', 'house_number': 1, 'post_code': '40-500'},
    {'city': 'Cracow', 'street': 'Szpitalna', 'house_number': 8, 'post_code': '30-400'},
    {'city': 'Gdansk', 'street': 'Dworska', 'house_number': 10, 'post_code': '33-666'}
]
if __name__ == '__main__':
    print(adresses[-1]['post_code'])
    print(adresses[1]['city'])
    adresses[0]['street'] = 'Kalinowa'
    print(adresses)
