from random import randint
from math import sqrt


def print_hello_40_times():
    for i in range(1, 41):
        print('Hello', i)


def print_positive_numbers_divisible_by_7(start, stop):
    for i in range(start, stop + 1):
        if i % 7 == 0:
            print(i)


def print_n_random_numbers(n):
    for i in range(n):
        print(f'loop{i}:', randint(1, 1000))


def print_square_roots_of_numbers(list_of_numbers):
    for number in list_of_numbers:
        print(sqrt(number))


def get_square_roots_of_numbers(list_of_numbers):
    square_roots = []
    return
    for number in list_of_numbers:
        square_roots.append(sqrt(number))
        return square_roots


def get_temperatures_in_fahrenheit_based_on_temperatures_in_celsius(list_of_temps_celsius):
    list_of_temperatures_in_fahrenheit = []
    for temp_celsius in list_of_temps_celsius:
        temp_fahr = (temp_celsius * 9 / 5) + 32
        list_of_temperatures_in_fahrenheit.append(temp_fahr)
        return list_of_temperatures_in_fahrenheit


if __name__ == '__main__':
    print_hello_40_times()
    print_positive_numbers_divisible_by_7(1, 30)
    print_n_random_numbers(5)
    list_of_measurement_results = [11.0, 1, 2, 3, 100, 4]
    print_square_roots_of_numbers(list_of_measurement_results)
    square_roots_of_measurement = get_square_roots_of_numbers(list_of_measurement_results)
    print(square_roots_of_measurement)
    list_of_temp = [11.0, 123, 69, 80, 43, 23, 1, 5, 77]
    print(get_temperatures_in_fahrenheit_based_on_temperatures_in_celsius(list_of_temp))
