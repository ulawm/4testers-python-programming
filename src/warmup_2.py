def get_square_of_number(input_number):
    return input_number ** 2


def convert_temp_celsius_to_fahrenheit(celsius_degree):
    return celsius_degree * 1.8 + 32


def calculate_rectangular_fluid(a, b, c):
    return a * b * c


if __name__ == '__main__':
    print(get_square_of_number(4))
    print(convert_temp_celsius_to_fahrenheit(20))
    print(calculate_rectangular_fluid(1, 2, 3))
