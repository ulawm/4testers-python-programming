import random

female_firstname = ['Kate', 'Agnes', 'Maria', 'Eryka', 'Anna', 'Ula']
male_firstname = ['John', 'Mark', 'Josh', 'Bob', 'Damian', 'Steve']
lastname = ['Major', 'Wojtycza', 'Smith', 'Nowak', 'Matoga', 'Suder']
countries = ['Poland', 'Germany', 'France', 'Italy', 'USA', "Slovakia"]


def generate_random_female_firstname(female_firstname):
    random_female_firstname = random.choice(female_firstname)
    return random_female_firstname


def generate_random_male_firstname(male_firstname):
    random_male_firstname = random.choice(male_firstname)
    return random_male_firstname


def generate_random_lastname(lastname):
    random_lastname = random.choice(lastname)
    return random_lastname


def generate_random_countries(countries):
    random_countries = random.choice(countries)
    return (random_countries)


def generate_random_email(random_firstname, random_lastname):
    domain = "example.com"
    return f'{random_firstname.lower()}.{random_lastname.lower()}@{domain}'


def generate_random_age():
    random_age = random.randint(5, 45)
    return random_age


def is_person_an_adult(random_age):
    if random_age >= 18:
        return True
    else:
        return False


def get_date_of_birth(random_age):
    return 2022 - random_age


def get_dictionary_with_random_female_personal_data():
    random_firstname = generate_random_female_firstname(female_firstname)
    random_lastname = generate_random_lastname(lastname)
    random_countries = generate_random_countries(countries)
    random_email = generate_random_email(random_firstname, random_lastname)
    random_age = generate_random_age()
    adult = is_person_an_adult(random_age)
    birth_date = get_date_of_birth(random_age)

    return {
        'firstname': random_firstname,
        'lastname': random_lastname,
        'country': random_countries,
        'email': random_email,
        'age': random_age,
        'adult': adult,
        'birth_year': birth_date,
    }


def get_dictionary_with_random_male_personal_data():
    random_firstname = generate_random_male_firstname(male_firstname)
    random_lastname = generate_random_lastname(lastname)
    random_countries = generate_random_countries(countries)
    random_email = generate_random_email(random_firstname, random_lastname)
    random_age = generate_random_age()
    adult = is_person_an_adult(random_age)
    birth_date = get_date_of_birth(random_age)
    return {
        'firstname': random_firstname,
        'lastname': random_lastname,
        'country': random_countries,
        'email': random_email,
        'age': random_age,
        'adult': adult,
        'birth_year': birth_date,
    }


def generate_list_of_dictionary_with_random_personal_data():
    list_of_dictionary_with_random_personal_data = []
    for i in range(0, 5):
        list_of_dictionary_with_random_personal_data.append(get_dictionary_with_random_female_personal_data())
        list_of_dictionary_with_random_personal_data.append(get_dictionary_with_random_male_personal_data())
    return list_of_dictionary_with_random_personal_data


def print_introduce_people_from_the_list(list_of_random_people):
    for people in range(len(list_of_random_people)):
        print(
            f"Hi! I'm {list_of_random_people[people]['firstname']} {list_of_random_people[people]['lastname']}. I come from {list_of_random_people[people]['country']} and I was born in {list_of_random_people[people]['birth_year']}")


if __name__ == '__main__':
    list_of_random_people = generate_list_of_dictionary_with_random_personal_data()
    print(list_of_random_people)
    print_introduce_people_from_the_list(list_of_random_people)
