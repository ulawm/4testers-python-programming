first_name = "Ula"
last_name = "Wojtycza-Major"
email = "ulawojtycza@interia.pl"
print("Mam na imię: ", first_name, ". ", "Moje nazwisko to: ", last_name, ".", sep="")

my_bio = ("Mam na imię: " + first_name + ". Moje nazwisko to: " + last_name + ". Mój email to: " + email + ".")
print(my_bio)

# F-string
my_bio_usinf_f_string = f"Mam na imię {first_name}. \nMoje nazwisko to {last_name}. \nMój email to {email}."
print(my_bio_usinf_f_string)

### Algebra ###
circle_radius = 10
area_of_a_circle_with_radius_5 = 3.14 * circle_radius * circle_radius
circumference_of_a_circle_with_radius_5 = 2 * 3.14 * circle_radius

print(area_of_a_circle_with_radius_5)
print(circumference_of_a_circle_with_radius_5)

long_matemathical_expression = 2 + 3 + 5 * 7 + 4 / 3 * (3 + 5 / 2) + 7**2 - 13
print(long_matemathical_expression)
