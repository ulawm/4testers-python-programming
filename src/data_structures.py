movies = ["Gladiator", "Holliday", "Red Sparrow", "Killer", "Armageddon"]

last_movie = movies[-1]
movies.append('LOTR')
movies.append('Titanic')

print(len(movies))
print(last_movie)
middle_movies = movies[2:5]
print(middle_movies)

movies.insert(0, 'Top Gun')
print(movies)

email = ['a@example.com', 'b@example.com']
print(len(email))
print(email[0])
print(email[-1])

friend = {
    "name": "Darek",
    "age": 32,
    "hobby": ["sport", "muzyka"]
}
friend_hobbies = friend["hobby"]
print("Hobbies of my friend:", friend_hobbies)
print(f"My friend has {len(friend_hobbies)} hobbies")

friend["hobby"].append("football")
print(friend)
friend["married"] = True
friend["age"] = 33
print(friend)



