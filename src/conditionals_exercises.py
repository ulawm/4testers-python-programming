def display_speed_information(speed):
    if speed > 50:
        print('Slow down :(')
    else:
        print('Thank you, your speed is OK :)')


def display_information_about_conditions(temperature, pressure):
    if temperature == 0 and pressure == 1013:
        return True
    else:
        return False


def print_the_value_of_speeding_fine_in_built_up_area(speed):
    print('Your speed was:', speed)
    if speed > 100:
        print('You lost your driving licenses')
    elif speed > 50:
        fine_amount = 500 + (speed - 10) * 10
        print(f'You are fine. Fine amount: {fine_amount}')
    else:
        print('Your speed is fine')


def display_rating(rating):
    if not (isinstance (rating, float) or isinstance (rating, int)):
        return 'N/A'
    elif rating < 2 or rating > 5:
        return 'N/A'
    elif rating >= 4.5:
        return 'bardzo dobry'
    elif rating >= 4:
        return 'dobry'
    elif rating >= 3:
        return 'dostateczny'
    else:
        return 'niedostateczny'


if __name__ == '__main__':
    display_speed_information(50)
    display_speed_information(49)
    display_speed_information(51)
    print(display_information_about_conditions(0, 1013))
    print(display_information_about_conditions(1, 1013))
    print(display_information_about_conditions(0, 1014))
    print(display_information_about_conditions(1, 1014))
    print(print_the_value_of_speeding_fine_in_built_up_area(101))
    print(print_the_value_of_speeding_fine_in_built_up_area(100))
    print(print_the_value_of_speeding_fine_in_built_up_area(50))
    print(print_the_value_of_speeding_fine_in_built_up_area(49))
    print(print_the_value_of_speeding_fine_in_built_up_area(51))
    print(display_rating('aaa'))
