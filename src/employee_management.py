import random
import time


def generate_random_number():
    return random.randint(0, 40)


def generate_random_email():
    domain = "example.com"
    names_list = ["james", "kate", "bill"]
    random_name = random.choice(names_list)
    suffix = time.time()
    return f'{random_name}.{suffix}@{domain}'


def generate_random_boolean():
    return random.choice([True, False])


def get_dictionary_with_random_personal_data():
    random_email = generate_random_email()
    random_number = generate_random_number()
    random_boolean = generate_random_boolean()
    return {
        "email": random_email,
        "seniority_years": random_number,
        "female": random_boolean
    }


def generate_list_of_dictionary_with_random_personal_data(number_of_dictionaries):
    list_of_dictionary = []
    for number in range(number_of_dictionaries):
        list_of_dictionary.append(get_dictionary_with_random_personal_data())
    return list_of_dictionary


if __name__ == '__main__':
    print(get_dictionary_with_random_personal_data())
    print(generate_list_of_dictionary_with_random_personal_data(10))
